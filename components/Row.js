import React, {} from 'react';

/**
 * Composant Row (represente une balise li de la liste des reponses possibles)
 */
export function Row(props){                
  function onRowClick(e){              //Appel la fonction selectAnswer du
      props.handleClick(props.item);   //composant App sur le composant Row sélectionné
  } 
  return(  
    <li className={props.classItem} onClick={onRowClick}>{props.item.reponse}</li>
  );   
}
