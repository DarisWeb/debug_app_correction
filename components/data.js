/**
 * dataQuizz: les données de chaque réponse
 * id: identifiant
 * value: réponse en entier
 * state: la classe css du composant Row (balise htmlt li)
 * reponse: la reponse en chaine de caracteres
 * isValid: 1 = bonne réponse; 0 = movaise réponse
 */
export const dataQuizz = [
    {
        id : 0,
        value: 4,
        state : "",
        reponse : "Quatre",
        isValid : 0
    },
    
    {
        id : 1,
        value: 8,
        state : "",
        reponse : "Huit",
        isValid : 0
    },
    {
        id : 2,
        value: 12,
        state : "",
        reponse : "Douze",
        isValid : 1         //La bonne réponse
    },
    {
        id : 3,
        value: 16,
        state : "",
        reponse : "Seize",
        isValid : 0
    }
]
