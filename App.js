import React, {useState} from 'react';
import { Row } from './components/Row';
import {dataQuizz} from './components/data';  //les donnees de chaque reponse
import './style.css';

// Composant App
function App() {
  let answer;     //reponse selectionnee du tableau data

  const [data, setData] = useState(dataQuizz);    //Tableau de donnees sur les reponses possibles
  const [response, setResponse] = useState('');   //reponse en texte affichee dans le DOM
  const [answered, setAnswered] = useState(null); //booleen indique si on a repondu
  
  /**
 * Sélectionne une réponse
 * @param item - l'element séléctionnee du tableau data 
 */
  function selectAnswer(item){
    if(!answered){  //si pas de réponse selectioné 
      
      const dataUpdate = data.map(itemMap => {  //on definit la classe css "selected"
        if(item.id === itemMap.id) {            //pour la li (composant Row) selectionnee
          item.state = "selected";
          return item;
        }
        itemMap.state = '';    //on definit la classe css vide pour les autres elements li
        return itemMap;                         
      })
      setData(dataUpdate);     //met à jour l'etat
    }
  } 

  /**
  * Valide la réponse et donne le résultat
  */
  function validateAnswer(){
    setAnswered(true);
    const dataUpdate = data.map(itemMap => {    
      if(itemMap.state === "selected") {
        answer = itemMap;                       //answer = element selectionnee  
      }
      return itemMap;
    })

    if(!answer){                            // si pas de champ selectionee
      setResponse("Rien n'est sélectionné");
    }
    else{
      if(answer.isValid === 1){         //si la reponse est bonne
        answer.state = "correct";       // on définit la class css de la ligne selectionnee
        setResponse('Bravo')            //on afiche le texte de la reponse

      }
      else{                             //si la reponse est movaise
        answer.state = "incorrect";     
        setResponse('Movaise réponse');
      }
    }
    setData(dataUpdate);                //met à jour l'etat
  }

  /**
   * Purge la réponse pour le recommencer
   */
  function resetAnswers(){
    const dataUpdate = data.map(itemMap => {    //met toute les class css à ''
      itemMap.state = '';
      return itemMap;
    });
    setResponse('');        //efface la reponse texte du DOM
    setAnswered(false);
    setData(dataUpdate);
    answer = null;
  }
  
  return (
    <div className="App">
      <div className="exercice">
        <h2>Quiz</h2>
        <h3>Combien d'erreurs comporte cette application ?</h3>
        <div> 
          <ul className="answers">
            { data.map((item) =>{ //Affichage des elements du tableau data 
              return (<Row item={item} classItem={item.state} handleClick={selectAnswer} key={item.id}/>);
            })}
          </ul>
        </div>
        <button className="validate" onClick={validateAnswer} >Répondre</button>
        <button className="reset" onClick={resetAnswers}>Recommencer</button>
      </div>
      <aside className="result">
        <h2>Résultat</h2>
        <p>{response}</p>   {/* ici on affiche la reponse texte */}
      </aside>
    </div>
  );
}

export default App;
